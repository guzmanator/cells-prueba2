(function(window) {
  'use strict';
  window.AppEnvironments = {
    'selected': {
      'app': 'default',
      'environment': 'env'
    },
    'availableApps': {
      'default': {
        'env': {
          'composerEndpoint': './composerMocks/',
          'coreCache': true
        }
      }
    }
  };
})(window);